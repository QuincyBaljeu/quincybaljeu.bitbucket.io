window.addEventListener("load", init);

function init() {
    document.getElementById('search').onkeydown = function (event) {
        if (event.keyCode == 13) {
            boekZoek();
        }
    }
}

function boekZoek() {
    var search = document.getElementById('search').value
    let results = document.getElementById('results')
    results.innerHTML = ""
    

 //functies zijn er om een error message te geven als er een eigenschap ontbreekt

    function showAmount(listPrice) {
        if (listPrice == null) {
            return " ";
        }
        else {
            return "Price: €" + listPrice.amount;
        }
    }

    function showAuthor(author) {
        if (author == null) {
            return "No author available";
        }
        else {
            return "By: " + author;
        }
    }

    function showDescription(description) {
        if (description == null) {
            return "No description available";
        }
        else {
            return description;
        }
    }

    function showLink(saleability) {
        if (saleability === "FOR_SALE") {
            return "This book is for sale on Google books!";
        }
        else if (saleability === "FREE") {
            return "This book is available for free on Google books";
        }
        else {
            return "This book is not available on Google books";
        }
    }

    function showThumbnail(smallThumbnail) {
        if (smallThumbnail == null) {
            return " ";
        }
        else {
            return smallThumbnail;
        }
    }

    function showPublisher(publisher) {
        if (publisher == null) {
            return " ";
        }
        else {
            return "Published by: " + publisher; 
        }
    }

    function showPublishedDate(publishedDate) {
        if (publishedDate == null) {
            return " ";
        }
        else {
            return "Release date: " + publishedDate;
        }
    }

    function showPages(pageCount) {
        if (pageCount == null) {
            return " ";
        }
        else {
            return "Pages: " + pageCount;
        }
    }

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",

        success: function (data) {
            for (i = 0; i < data.items.length; i++) {
                let resultTable = document.createElement("div")
                resultTable.setAttribute("class", "resultTable")
                results.appendChild(resultTable)

                
                resultTable.innerHTML += "<img src='" + showThumbnail(data.items[i].volumeInfo.imageLinks.smallThumbnail)
                + "'></img>"
                resultTable.innerHTML += "<h1>" + data.items[i].volumeInfo.title + "</h1>"
                resultTable.innerHTML += "<h2>" + showAuthor(data.items[i].volumeInfo.authors) + "</h2>"
                resultTable.innerHTML += "<h2>" + showPublisher(data.items[i].volumeInfo.publisher) + "</h2>"
                resultTable.innerHTML += "<h2>" + showPublishedDate(data.items[i].volumeInfo.publishedDate) + "</h2>"
                resultTable.innerHTML += "<h2>" + showPages(data.items[i].volumeInfo.pageCount) + "</h2>"
                resultTable.innerHTML += "<h3>" + showDescription(data.items[i].volumeInfo.description) + "</h3>"
                resultTable.innerHTML += "<h4>" + showLink(data.items[i].saleInfo.saleability)
                + "<a href='" + (data.items[i].saleInfo.buyLink) + "'> " + showAmount(data.items[i].saleInfo.listPrice)
                + "</a></h4>"

            }
        },
        type: 'GET'
    });
}

document.getElementById('button').addEventListener('click', boekZoek, false)

//lekker ronnie//